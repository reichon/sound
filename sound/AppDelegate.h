//
//  AppDelegate.h
//  sound
//
//  Created by Rei K on 2016/06/10.
//  Copyright © 2016年 Rei K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

