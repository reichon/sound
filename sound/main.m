//
//  main.m
//  sound
//
//  Created by Rei K on 2016/06/10.
//  Copyright © 2016年 Rei K. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
